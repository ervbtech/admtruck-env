#!/bin/bash

if [ ! -d /admtruck-api ]; then
  git clone https://erickbarbosa@bitbucket.org/ervbme/admtruck-api.git
fi

cd /admtruck-api
git pull
find /admtruck-api -name node_modules -exec rm -rf {} \;

if [ -d .git ]; then
  rm -rf .git
fi

npm install
npm install nodemon
npm start --no-daemon
