#!/bin/bash

if [ ! -d /admtruck-site ]; then
  git clone https://erickbarbosa@bitbucket.org/ervbme/admtruck-site.git
fi

cd /admtruck-site
git pull

npm install
npm start
