#!/bin/bash

if [ ! -d /admtruck-web ]; then
  git clone https://erickbarbosa@bitbucket.org/ervbme/admtruck-web.git
fi

cd /admtruck-web
git pull
find /admtruck-web -name node_modules -exec rm -rf {} \;

if [ -d .git ]; then
  rm -rf .git
fi

npm install
npm install -g gulp
npm install -g jspm
npm install -g jspm-bower-endpoint
npm install jspm-bower-endpoint

jspm registry create bower jspm-bower-endpoint
jspm registry config github
jspm install

gulp serve --no-daemon
